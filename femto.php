<?php
/**
 * femto.php
 *
 * PHP version 5
 *
 * @author    Rolf Strathewerd <rolf@scaldra.net>
 * @copyright 2018 Rolf Strathewerd
 * @license   GNU General Public License v3.0.
 * @link
 *
 */
const FILENAME = "/tmp/log";
const MAXLOGSIZE = 100;

$x        = file_get_contents(FILENAME);
if ($x === false) {
    $a = [];
} else {
    $a = json_decode($x, true);
}
if (!empty($_POST['log'])) {
    $a[] = date("d.m.Y H:i:s  ")  . $_POST['log'];
    if (count($a) > 100) {
        array_shift($a);
    }
    file_put_contents(FILENAME, json_encode($a));
} else {
    echo "<html><body><pre>";
    $a = array_reverse($a);
    foreach ($a as $detail) {
        echo $detail . "<br>";
    }
    echo "</pre></body></html>";
}