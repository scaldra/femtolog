# femtolog

Most minimal logging


Put it anywhere on a webserver with PHP and add log-messages

```bash
curl --data "log=A sample log message" https://YOURSERVER/femto.php
```
 
```bash 
#!/bin/bash
function femto {
    curl --data "log=$1" https://YOURSERVER/femto.php
                }

femto "A sample log message"
```